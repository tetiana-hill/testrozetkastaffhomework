import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class Lesson_24 {
    WebDriver driver;
    WebDriverWait wait;

    @BeforeMethod
    public void before() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\user\\IdeaProjects\\FirstProjectForAutomation\\drivers\\chromedriver.exe");
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver, 10);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("https://rozetka.com.ua");
        //driver.get("https://google.com.ua");
    }

    /*@Test
    public void testCheckbox() throws InterruptedException {
        WebElement login = driver.findElement(By.xpath("//a[@class='header-topline__user-link link-dashed']"));

        login.click();

        WebElement rememberCheckboxInput = driver.findElement(By.id("remember_me"));
        WebElement rememberCheckbox = driver.findElement(By.xpath("//label[@class='auth-modal__remember-checkbox']"));

        System.out.println(rememberCheckboxInput.isSelected());

        rememberCheckbox.click();

        System.out.println(rememberCheckboxInput.isSelected());
    }*/


    /*@Test
    public void testClear() throws InterruptedException {
        WebElement searchInput = driver.findElement(By.name("search"));
        searchInput.sendKeys("Bla bla bla");

        TimeUnit.SECONDS.sleep(10);

        searchInput.clear();

        TimeUnit.SECONDS.sleep(10);
    }*/

    /*@Test
    public void testOption() throws InterruptedException {

        selectOption(driver, "866");

        TimeUnit.SECONDS.sleep(10);
    }

    public static void selectOption(WebDriver driver, String option) {
        WebElement makerList = driver.findElement(By.id("form_maker_id"));
        String makerListOption = String.format("//select[@id='form_maker_id']/optgroup[2]/option[@value='%s']", option);

        makerList.click();
        driver.findElement(By.xpath(makerListOption)).click();
    }*/

    /*@Test
    public void javaScript() throws InterruptedException {

        JavascriptExecutor jse = (JavascriptExecutor)driver;
        jse.executeScript("alert('Are you sure?');");

        TimeUnit.SECONDS.sleep(10);

        driver.switchTo().alert().accept();

        TimeUnit.SECONDS.sleep(10);
    }*/

    /*@Test
    public void windowsHandlesExp() throws InterruptedException {
        WebElement loginBtn = driver.findElement(By.xpath("//a[@class='header-topline__user-link link-dashed']"));
        loginBtn.click();


        WebElement regBtn = driver.findElement(By.xpath("//a[@class='auth-modal__register-link']"));
        regBtn.click();


        WebElement privatePolicyBtn = driver.findElement(By.xpath("//a[@class='button button_type_link']"));
        privatePolicyBtn.click();

        String mainTab = driver.getWindowHandle();

        for (String tab : driver.getWindowHandles()) {
            driver.switchTo().window(tab);
        }

        TimeUnit.SECONDS.sleep(5);

        driver.switchTo().window(mainTab);

        TimeUnit.SECONDS.sleep(5);

    }*/

    //Проверка отсутствия элемента на странице
    @Test
    public void elementFound() {
        List<WebElement> loginBtn = driver.findElements(By.xpath("//a[@class='header-topline__user-link link-dashed']"));

        if (loginBtn.size() > 0) {
            System.out.println("Login button appeared");
        } else {
            Assert.fail();
        }
    }

    @AfterMethod
    public void after() {
        driver.quit();
    }
}
